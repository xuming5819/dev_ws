#include <iostream>                         // c++ 接口库
#include <chrono>                           // 关于时间的头文件
#include <memory>                           // 关于指针的头文件
#include <rclcpp/rclcpp.hpp>                // ROS2 C++ 接口库
#include <std_msgs/msg/string.hpp>          // 消息类型的头文件 


/*
创建一个发布节点
*/

using namespace std::chrono_literals;

class PublisherNode:public rclcpp::Node{
public:
PublisherNode():rclcpp::Node("topic_helloworld_pub"), m_count(0)
{
    publisherPtr = this->create_publisher<std_msgs::msg::String>("topic", 10); 
    // 对发布者 publisherPtr 初始化，消息类型定义为 String, 话题名为 topic， 话题的消息缓冲池长度为 10
    timerPtr = this->create_wall_timer(500ms, std::bind(&PublisherNode::timer_callback, this));
    // 初始化一个定时器每隔 500ms 执行一次 timer_callback 函数

}

private:
void timer_callback()
{
    auto msg = std_msgs::msg::String();
    msg.data = "Hello, World! " + std::to_string(m_count++);
    RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", msg.data.c_str());
    publisherPtr->publish(msg);  
}

private:
size_t m_count;
rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisherPtr;       // 声明发布者指针对象
rclcpp::TimerBase::SharedPtr timerPtr;              // 声明定时器

};



int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);                            // ROS2 C++ 接口初始化
    rclcpp::spin(std::make_shared<PublisherNode>());     // 开始处理回调
    rclcpp::shutdown();                                  // 终止程序

    return 0;
}