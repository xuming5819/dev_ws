#include <iostream>                         // c++ 接口库
#include <chrono>                           // 关于时间的头文件
#include <memory>                           // 关于指针的头文件
#include <rclcpp/rclcpp.hpp>                // ROS2 C++ 接口库
#include <std_msgs/msg/string.hpp>          // 消息类型的头文件 


using std::placeholders::_1;

class SubscriberNode:public rclcpp::Node{
public:
    SubscriberNode():rclcpp::Node("topic_helloworld_sub")
    {
        subscriptionPtr = this->create_subscription<std_msgs::msg::String>("topic", 10, std::bind(&SubscriberNode::topic_callback, this, _1));
    }

public:
void topic_callback(const std_msgs::msg::String::SharedPtr msg) const
{
    RCLCPP_INFO(this->get_logger(), "I heard: %s", msg->data.c_str());

}

private:

rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscriptionPtr;

};




int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<SubscriberNode>());
    rclcpp::shutdown();
    return 0;
}