#include <iostream>             // c++ 的接口库
#include <rclcpp/rclcpp.hpp>    // ROS2 C++ 接口库



/*
@作者: Ming Xu
@说明: ROS2节点示例-发布“Hello World”日志信息, 使用面向过程的实现方式
*/

int main(int argc, char** argv)                                  // ROS2 节点主入口 main 函数
{
    rclcpp::init(argc, argv);                                    // ROS2 C++ 接口初始化
    auto node =  rclcpp::Node::make_shared("node_helloworld");   // 创建 ROS2 节点对象并进行初始化
    rclcpp::Rate loop_rate(10.0);                                // 创建 10 ms     
    while (rclcpp::ok())
    {
        RCLCPP_INFO(node->get_logger(), "Hello World");          // ROS2 日志输出
        loop_rate.sleep();                                       // 休眠控制循环时间
    }   
    rclcpp::spin(node);                                          // 绑定启动节点
    rclcpp::shutdown();                                          // 关闭 ROS2 C++ 接口

    return 0;
}