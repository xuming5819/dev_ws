/*
@作者: Ming Xu
@说明: ROS2节点示例-发布“Hello World”日志信息, 使用面向对象的实现方式
*/


#include <iostream>             // c++ 的接口库
#include <rclcpp/rclcpp.hpp>    // ROS2 C++ 接口库


class HelloWorldNode:public rclcpp::Node{
public:
    HelloWorldNode()
    :rclcpp::Node("node_helloworld_class")
    {   rclcpp::Rate loop_rate(500);
        while (rclcpp::ok())
        {
            RCLCPP_INFO(this->get_logger(), "Hello World");          // ROS2 日志输出
            loop_rate.sleep();                                       // 休眠控制循环时间
        }
    }

};


int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<HelloWorldNode>());
    rclcpp::shutdown();
    return 0;
}